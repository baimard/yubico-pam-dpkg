# Italian translation for yubico-pam debconf templates.
# Copyright (C) 2012, the yubico-pam copyright holder
# This file is distributed under the same license as the yubico-pam package.
# Francesca Ciceri, <madamezou@zouish.org>, 2012-2014.
#
msgid ""
msgstr ""
"Project-Id-Version: yubico-pam\n"
"Report-Msgid-Bugs-To: yubico-pam@packages.debian.org\n"
"POT-Creation-Date: 2012-01-18 07:24+0100\n"
"PO-Revision-Date: 2014-03-12 18:52+0100\n"
"Last-Translator: Francesca Ciceri <madamezou@zouish.org>\n"
"Language-Team: Italian <debian-l10n-italian@lists.debian.org>\n"
"Language: Italian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../libpam-yubico.templates:2001
msgid "Parameters for Yubico PAM:"
msgstr "Parametri per il modulo PAM Yubico:"

#. Type: string
#. Description
#: ../libpam-yubico.templates:2001
msgid ""
"The Yubico PAM module supports two modes of operation: online validation of "
"YubiKey OTPs or offline validation of YubiKey HMAC-SHA-1 responses to "
"challenges."
msgstr ""
"Il modulo PAM Yubico supporta due modalità operative: autenticazione "
"online delle YubiKey OTP o autenticazione offline di YubiKey HMAC-SHA-1 con "
"funzionalità challenge response."

#. Type: string
#. Description
#: ../libpam-yubico.templates:2001
msgid ""
"The default is online validation, and for that to work you need to get a "
"free API key at https://upgrade.yubico.com/getapikey/ and enter the key id "
"as \"id=NNNN\" and the base64 secret as \"key=...\"."
msgstr ""
"La modalità predefinita è l'autenticazione online e perché funzioni è "
"necessario ottenere una chiave API gratuita all'indirizzo "
"https://upgrade.yubico.com/getapikey/ e inserire l'identificativo della "
"chiave nel formato «id=NNNN» e la chiave di sessione in base64 come "
"«key=...»."

#. Type: string
#. Description
#: ../libpam-yubico.templates:2001
msgid ""
"All the available parameters for the Yubico PAM module are described in /usr/"
"share/doc/libpam-yubico/README.gz. To avoid accidental lock-outs the module "
"will not be active until it is enabled with the \"pam-auth-update\" command."
msgstr ""
"Tutti i parametri disponibili per il modulo Yubico PAM sono descritti in "
"/usr/share/doc/libpam-yubico/README.gz. Per evitare che l'utente sia chiuso "
"accidentalmente fuori dal proprio sistema, il modulo PAM Yubico rimarrà "
"inattivo finché non abilitato con il comando «pam-auth-update»."
